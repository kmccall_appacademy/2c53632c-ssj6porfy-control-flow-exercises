# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  lowercase_letters = ("a".."z").to_a
  str.each_char do |char|
    str.delete!(char) if lowercase_letters.include?(char)
  end
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  length_of_str = str.length
  middle = length_of_str / 2
  length_of_str.odd? ? str[middle] : str[(middle - 1)..middle]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  vowel_count = 0
  str.each_char do |char|
    vowel_count += 1 if VOWELS.include?(char.downcase)
  end
  vowel_count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).to_a.inject(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  new_str = ""
  arr.each { |el| new_str += el + separator }
  new_str = new_str[0..-2] if new_str[-1] == separator
  new_str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  new_str = ""
  str.chars.each_index do |idx|
    if (idx + 1).odd?
      new_str += str[idx].downcase
    else
      new_str += str[idx].upcase
    end
  end
  new_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  word_arr = str.split(' ')
  word_arr.map { |word| word.length > 4 ? word.reverse : word }.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  nums_arr = (1..n).to_a
  nums_arr.map do |num|
    if (num % 3).zero? && (num % 5).zero?
      'fizzbuzz'
    elsif (num % 3).zero?
      'fizz'
    elsif (num % 5).zero?
      'buzz'
    else
      num
    end
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  new_arr = []
  idx = arr.size - 1
  while idx >= 0
    new_arr << arr[idx]
    idx -= 1
  end
  new_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num < 2
  numbers = (2...num).to_a
  numbers.each { |number| return false if (num % number).zero? }
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  numbers = (1..num).to_a
  sorted_factors = []
  numbers.each { |number| sorted_factors << number if (num % number).zero? }
  sorted_factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select {|number| prime?(number) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).size
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  index_hash = { 'even' => [], 'odd' => [] }
  arr.each do |el|
    el.odd? ? index_hash['odd'] << el : index_hash['even'] << el
  end
  index_hash.select { |key, value| value.size == 1 }.values.first[0]
end
